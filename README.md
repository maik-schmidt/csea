# CSEA - Continuous Spectrum Estimation Algorithm

Deconvolution algorithms for Cherenkov astronomy and other use cases.


## Getting Started

The algorithm is based on [DSEA\+](https://github.com/mirkobunse/CherenkovDeconvolution.py) as described in my [Master's thesis](https://www-ai.cs.tu-dortmund.de/auto?self=%24Publication_fw1n66po1s).
This package is documented on the [docs site](https://maik-schmidt.bitbucket.io/csea).
You can install it by cloning the repository and running
```
pip3 install -r requirements.txt
pip3 install -e .
```
