#
# csea
# Copyright 2019 Maik Schmidt
#
#
# Deconvolution methods for Cherenkov astronomy and other use cases in experimental physics.
#
#
# csea is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with csea. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from scipy.special import gamma
from scipy.stats import iqr
from sklearn.neighbors import KernelDensity


class DensityEstimator:
    def __init__(self):
        pass

    def fit(self, X, X_std=None):
        pass

    def predict(self, X=None):
        pass


class Histogram(DensityEstimator):
    """Compute the histogram of a set of data

    Attributes
    ----------
    weighted : bool, optional
        Whether to weight the samples based on uncertainty, defaults to False.
    bins : int or ndarray, optional
        If bins is int, it defines the number of equal-width bins in the given range (20, by default).
        If bins is ndarray, it defines a monotonically increasing array of bin edges, including the
        rightmost edge, allowing for non-uniform bin widths.
    I : int, optional
        Number of predicted samples, defaults to 1000
    """

    def __init__(self, weighted=False, bins=20, I=1000):
        self.bins = bins
        self.weighted = weighted
        self.bin_dens = None
        self.X = None
        self.I = I

    def fit(self, X, X_std=None):
        """Fit the Kernel Density model on the data.

        Parameters
        ----------
        X : array_like, shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.
        X_std : array_like, shape (n_samples,), optional
            Standard deviation at X. Must be specified if uncertainty bandwidth
            was chosen.
        """
        if isinstance(self.bins, int):
            self.bins = np.linspace(np.min(X), np.max(X), self.bins + 1)

        if X_std is not None and self.weighted:
            weights = _weights_from_uncertainty(X_std)
        else:
            weights = np.repeat([1], len(X)) / len(X)
        self.bin_dens = np.bincount(np.digitize(X, self.bins), weights=weights, minlength=len(self.bins) + 1)
        # Normalize so the are of each bin contains the respective probability
        self.bin_dens = self.bin_dens[1:-1] * (len(self.bins) - 1) / (
                (self.bins[-1] - self.bins[0]) * np.sum(self.bin_dens[1:-1]))
        return self

    def predict(self, X=None):
        """Evaluate the histogram on the data.

        Parameters
        ----------
        X : ndarray, shape (n_samples,), optional
            An array of points to query. If none is given
            defaults to self.I many equidistant values
        Returns
        -------
        X : ndarray, shape (n_samples,)
            the query points
        y : ndarray, shape (n_samples,)
            The array of density evaluations. These are normalized to be
            probability densities, so values will be low for high-dimensional
            data.
        """
        if X is None:
            X = np.linspace(self.bins[0], self.bins[-1], self.I)

        y = self.bin_dens[np.digitize(X, self.bins[1:-1])]
        return X, y


class KernelDensityEstimator(DensityEstimator):
    r"""Kernel Density Estimation

    Attributes
    ----------
    weighted : bool, optional
        Whether to weight the samples based on uncertainty, defaults to False.
    kernel : string
        The kernel to use.  Valid kernels are
        ['gaussian'|'tophat'|'epanechnikov'|'exponential'|'linear'|'cosine']
        Default is 'gaussian'.
    bandwidth : float or string
        The bandwidth of the kernel. 'silverman' estimates the bandwidth based
        on the training data. 'uncertainty_add' and 'uncertainty_mult'
        calculate individual bandwidth for each sample based on an Silverman's
        bandwidth and the uncertainty of each sample. Uncertainty bandwidth
        only support gaussian and epanechnikov kernel.
    method : float or string
        'uncertainty_add' and 'uncertainty_mult' calculate individual bandwidth
        for each sample based on the bandwidth parameter and the uncertainty of each sample.
        Both only support gaussian and epanechnikov kernel.
    silverman_factor : float or string
        The preceding factor in Silverman's rule of thumb, defaults to 0.9.
    use_iqr : bool
        Whether to use the robust estimator for the standard deviation using
        the interquartile range. This is knows as the better rule-of-thumb.
        Defaults to True.
    I : int
        Number of predicted samples
    normalize : int
        Whether data is normalized. Use the number of bins (default: 20)
    normalize_uncertainties : bool
        Whether uncertainties are normalized to have mean 1:
        $$\sigma' = {\sigma\over np.mean(\sigma)}$$
    """

    def __init__(self, weighted=False, kernel='gaussian', bandwidth='silverman', method=None, silverman_factor=0.9,
                 use_iqr=True, I=1000, normalize=20, normalize_uncertainties=True):
        self.kernel = kernel
        self.bandwidth = bandwidth
        self.method = bandwidth if type(bandwidth) is str else method
        self.I = 1000
        self.model = None
        self.weighted = weighted
        self.silverman_factor = silverman_factor
        self.use_iqr = use_iqr
        self.normalize = normalize
        self.normalize_uncertainties = normalize_uncertainties
        self.X = None
        self.X_std = None

    def fit(self, X, X_std=None):
        """Fit the Kernel Density model on the data.

        Parameters
        ----------
        X : array_like, shape (n_samples, n_features)
            List of n_features-dimensional data points. Each row
            corresponds to a single data point.
        X_std : array_like, shape (n_samples,), optional
            Standard deviation at X. Must be specified if uncertainty bandwidth
            was chosen.
        """
        self.X = X
        self.X_std = None
        if X_std is not None:
            self.X_std = X_std / np.mean(X_std) if self.normalize_uncertainties else X_std

        if self.bandwidth in ['silverman', 'uncertainty_add', 'uncertainty_mult']:
            self.bandwidth = _silverman(X, self.silverman_factor, self.use_iqr, kernel=self.kernel,
                                       normalize=self.normalize)
            #print('Silverman bandwidth: {}'.format(self.bandwidth))

        if X_std is None or self.method not in ['uncertainty_add', 'uncertainty_mult']:
            sample_weight = None
            if X_std is not None and self.weighted:
                sample_weight = _weights_from_uncertainty(X_std)
                sample_weight[sample_weight <= 0] = np.finfo(float).eps
            self.model = KernelDensity(kernel=self.kernel, bandwidth=self.bandwidth)
            self.model.fit(X[:, np.newaxis], sample_weight=sample_weight)

        return self

    def predict(self, X=None):
        """Evaluate the density model on the data.

        Parameters
        ----------
        X : ndarray, shape (n_samples,), optional
            An array of points to query. If none is given
            defaults to self.I many equidistant values
        Returns
        -------
        X : ndarray, shape (n_samples,)
            the query points
        y : ndarray, shape (n_samples,)
            The array of density evaluations. These are normalized to be
            probability densities, so values will be low for high-dimensional
            data.
        """
        if X is None:
            X = np.linspace(np.min(self.X) - 2 * self.bandwidth, np.max(self.X) + 2 * self.bandwidth, self.I)

        if self.X_std is None or self.method not in ['uncertainty_add', 'uncertainty_mult']:
            log_y = self.model.score_samples(X[:, np.newaxis])
            y = np.exp(log_y)

        else:
            if self.method == 'uncertainty_add':
                bandwidths = self.bandwidth + self.X_std
                #print(bandwidths)

            elif self.method == 'uncertainty_mult':
                bandwidths = self.bandwidth * (1 + self.X_std)
                #print(bandwidths)
            else:
                raise NotImplementedError

            y = np.zeros((len(self.X), len(X)))
            if self.kernel == 'gaussian':
                for i in range(len(self.X)):
                    y[i, :] = np.exp(-((self.X[i] - X) / bandwidths[i]) ** 2 / 2) / (np.sqrt(2 * np.pi) * bandwidths[i])
            elif self.kernel == 'epanechnikov':
                for i in range(len(self.X)):
                    y[i, :] = 3 / 4 * (1 - ((self.X[i] - X) / bandwidths[i]) ** 2) / bandwidths[i]
                y[y < 0] = 0
            else:
                raise NotImplementedError
            y = np.mean(y, axis=0)

        return X, y


# Silverman's rule of thumb
def _silverman(X, silverman_factor, use_iqr, ddof=1, kernel=None, normalize=None):
    s = np.std(X, axis=0, ddof=ddof)
    if use_iqr:
        s = np.minimum(s, iqr(X, axis=0) / 1.34)
    if X.ndim <= 1:
        bandwidth = silverman_factor * s * len(X) ** (-1 / 5)
    else:
        d = X.shape[1]
        s = np.mean(s)
        if kernel == 'gaussian':
            k = (4 / (2 * d + 1)) ** (1 / (d + 4))
        elif kernel == 'epanechnikov':
            c = np.pi ** (d / 2) / gamma(1 + d / 2)
            k = ((8 * d * (d + 2) * (d + 4) * (2 * np.sqrt(np.pi)) ** d) / ((2 * d + 1) * c)) ** (1 / (d + 4))
        else:
            raise NotImplementedError
        bandwidth = s * k * len(X) ** (-1 / (d + 4))

    if normalize is not None:
        bandwidth = bandwidth * normalize / ((np.max(X) - np.min(X)) / 2)

    if bandwidth <= 0:
        bandwidth = np.finfo(float).eps

    return bandwidth


# Convert uncertainty to weights
def _weights_from_uncertainty(uncertainty):
    weights = np.exp(-uncertainty)
    weights = weights / np.sum(weights)
    return weights
