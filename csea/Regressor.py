#
# csea
# Copyright 2019 Maik Schmidt
#
#
# Deconvolution methods for Cherenkov astronomy and other use cases in experimental physics.
#
#
# csea is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with csea. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from csea.DensityEstimator import _silverman
from sklearn import gaussian_process
from sklearn.base import clone
from sklearn.kernel_ridge import KernelRidge
from sklearn.neighbors import KernelDensity
from sklearn.utils.validation import check_X_y

try:
    from skgarden import mondrian
except ImportError:
    print('No module named \'skgarden\'. To use Mondrian trees and forests run: pip3 install scikit-garden')


class Regressor:
    def __init__(self):
        pass

    def fit(self, X, y, sample_weight=None):
        pass

    def predict_proba(self, X):
        pass


class GaussianProcessRegressor(Regressor):
    """Gaussian process regression (GPR).

    The implementation is based on Algorithm 2.1 of Gaussian Processes
    for Machine Learning (GPML) by Rasmussen and Williams.

    Attributes
    ----------
    **kwargs : optional
        Keywords passed to the scikit-learn Gaussian process.

    """

    def __init__(self, **kwargs):
        self.model = gaussian_process.GaussianProcessRegressor(normalize_y=True, **kwargs)
        self.alpha = self.model.alpha

    def fit(self, X, y, sample_weight=None):
        """Fit Gaussian process regression model.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data
        y : array-like, shape = (n_samples, [n_output_dims])
            Target values
        sample_weight : array-like, shape = [n_samples] or None
            Sample weights. If None, then samples are equally weighted.
        Returns
        -------
        self : returns an instance of self.
        """
        if sample_weight is not None:
            sample_weight = sample_weight / np.sum(sample_weight)
            self.model.alpha = self.alpha / sample_weight
        self.model.fit(X, y)
        return self

    def predict_proba(self, X):
        """Predict using the Gaussian process regression model
        We can also predict based on an unfitted model by using the GP prior.
        In addition to the mean of the predictive distribution, also its
        standard deviation.

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Query points where the GP is evaluated
        Returns
        -------
        y_pred : array, shape = (n_samples, [n_output_dims])
            Mean of predictive distribution a query points
        y_std : array, shape = (n_samples,), optional
            Standard deviation of predictive distribution at query points.
        """
        y_pred, y_std = self.model.predict(X, return_std=True)
        return y_pred, y_std


class MondrianTreeRegressor(Regressor):
    """A Mondrian tree.

    The splits in a mondrian tree regressor differ from the standard regression
    tree in the following ways.

    At fit time:
        - Splits are done independently of the labels.
        - The candidate feature is drawn with a probability proportional to the
          feature range.
        - The candidate threshold is drawn from a uniform distribution
          with the bounds equal to the bounds of the candidate feature.
        - The time of split is also stored which is proportional to the
          inverse of the size of the bounding-box.

    At prediction time:
        - Every node in the path from the root to the leaf is given a weight
          while making predictions.
        - At each node, the probability of an unseen sample splitting from that
          node is calculated. The farther the sample is away from the bounding
          box, the more probable that it will split away.
        - For every node, the probability that an unseen sample has not split
          before reaching that node and the probability that it will split away
          at that particular node are multiplied to give a weight.

    Attributes
    ----------
    **kwargs : optional
        Keywords passed to the scikit-garden Mondrian tree.

    """

    def __init__(self, **kwargs):
        self.model = mondrian.MondrianTreeRegressor(**kwargs)

    def fit(self, X, y, sample_weight=None):
        """Builds a trees from the training set (X, y).

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples.
        y : array-like, shape = [n_samples] or [n_samples, n_outputs]
            The target values.
        sample_weight : array-like, shape = [n_samples] or None
            Sample weights. If None, then samples are equally weighted. Splits
            that would create child nodes with net zero or negative weight are
            ignored while searching for a split in each node.
        Returns
        -------
        self : object
            Returns self.
        """
        self.model.fit(X, y, sample_weight)
        return self

    def predict_proba(self, X):
        """Predict regression value for X.

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The input samples.
        Returns
        -------
        y_pred : array-like, shape = (n_samples,)
            Predictions at X.
        y_std : array-like, shape = (n_samples,)
            Standard deviation at X.
        """
        y_pred, y_std = self.model.predict(X, return_std=True)
        return y_pred, y_std


class MondrianForestRegressor(Regressor):
    """A Mondrian forest.

    A MondrianForestRegressor is an ensemble of MondrianTreeRegressors.
    The variance in predictions is reduced by averaging the predictions
    from all trees.

    Attributes
    ----------
    **kwargs : optional
        Keywords passed to the scikit-garden Mondrian forest.

    """

    def __init__(self, **kwargs):
        self.model = mondrian.MondrianForestRegressor(**kwargs)

    def fit(self, X, y, sample_weight=None):
        """Builds a forest of trees from the training set (X, y).

        Parameters
        ----------
        X : array-like or sparse matrix of shape = [n_samples, n_features]
            The training input samples. Internally, its dtype will be converted
            to ``dtype=np.float32``. If a sparse matrix is provided, it will be
            converted into a sparse ``csc_matrix``.
        y : array-like, shape = [n_samples] or [n_samples, n_outputs]
            The target values (class labels in classification, real numbers in
            regression).
        sample_weight : array-like, shape = [n_samples] or None
            Sample weights. If None, then samples are equally weighted. Splits
            that would create child nodes with net zero or negative weight are
            ignored while searching for a split in each node.
        Returns
        -------
        self : object
            Returns self.
        """
        # Parameter sample_weight is not passed in the currect version of scikit-garden
        # This workaround just uses the superclass that passes the parameter correctly
        X, y = check_X_y(X, y, dtype=np.float32, multi_output=False)
        super(mondrian.MondrianForestRegressor, self.model).fit(X, y, sample_weight)
        return self

    def predict_proba(self, X):
        r"""
        Returns the predicted mean and std.
        The prediction is a GMM drawn from
        \(\sum_{i=1}^T w_i N(m_i, \sigma_i)\) where \(w_i = {1 \over T}\).
        The mean \(E[Y | X]\) reduces to \(\sum_{i=1}^T m_i \over T\)
        The variance \(Var[Y | X]\) is given by
        $$Var[Y | X] = E[Y^2 | X] - E[Y | X]^2\\
        ={\sum_{i=1}^T E[Y^2_i| X]\over T} - E[Y | X]^2\\
        ={\sum_{i=1}^T (Var[Y_i | X] + E[Y_i | X]^2)\over T} - E[Y| X]^2$$

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Input samples.
        Returns
        -------
        y_pred : array-like, shape = (n_samples,)
            Predictions at X.
        y_std : array-like, shape = (n_samples,)
            Standard deviation at X.
        """
        y_pred, y_std = self.model.predict(X, return_std=True)
        return y_pred, y_std


class KernelRidgeRegressor(Regressor):
    """
    Kernel ridge regression (KRR) combines ridge regression (linear least squares
    with l2-norm regularization) with the kernel trick. It thus learns a linear function
    in the space induced by the respective kernel and the data. For non-linear kernels,
    this corresponds to a non-linear function in the original space.

    Attributes
    ----------
    kernel : str, optional
        The Kernel used, defaults to 'rbf'.
    kernel_density_estimator : str, optional
        The kernel used for the density estimator, defaults to 'gaussian'.
    **kwargs : optional
        Keywords passed to the scikit-garden Mondrian forest.

    """

    def __init__(self, kernel='rbf', kernel_density_estimator='gaussian', **kwargs):
        self.model = KernelRidge(alpha=0.1, kernel=kernel, **kwargs)
        self.density_estimator = None
        self.kernel_density_estimator = kernel_density_estimator
        self.y_mean = None
        self.s = None

    def fit(self, X, y, sample_weight=None):
        """Fit Kernel Ridge regression model

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data
        y : array-like, shape = (n_samples, [n_output_dims])
            Target values
        sample_weight : array-like, shape = [n_samples] or None
            Individual weights for each sample, ignored if None is passed.
        Returns
        -------
        self : object
            Returns self.
        """
        self.y_mean = np.mean(y)
        self.model.fit(X, y - self.y_mean, sample_weight)
        self.s = np.std(self.model.predict(X) + self.y_mean - y)
        self.density_estimator = KernelDensity(bandwidth=_silverman(X, None, True, kernel=self.kernel_density_estimator),
                                               kernel=self.kernel_density_estimator).fit(X, sample_weight)
        return self

    def predict_proba(self, X):
        """Predict using the kernel ridge model

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Samples
        Returns
        -------
        y_pred : array-like, shape = (n_samples,)
            Predicted values
        y_std : array-like, shape = (n_samples,)
            Haerdle's asyptotic standard deviation
        """
        y_pred = self.model.predict(X) + self.y_mean
        X_dens = np.exp(self.density_estimator.score_samples(X))
        k = 1 / (2 * np.sqrt(np.pi))
        y_std = self.s * np.sqrt(k / (X_dens + np.finfo(float).eps))
        y_std = np.log10(y_std + 1)
        return y_pred, y_std


class CompositeRegressor(Regressor):
    """
    The composite regression combines a regression model that predicts the outputs
    and a second regression model predicting the uncertainty of the first model.

    Attributes
    ----------
    model : regressor
        The model predicting the output.
        Use any scikit-learn regressor.
    uncertainty_model : regressor, optional
        The model predicting the uncertainty of self.model.
        Use any scikit-learn regressor.
        Defaults to None, which return a constant uncertainty of 1.

    """

    def __init__(self, model, uncertainty_model=None):
        self.model = model
        self.uncertainty_model = clone(model) if uncertainty_model == 'same' else uncertainty_model
        self.prior = None

    def fit(self, X, y, sample_weight=None):
        """Fit both regression models

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Training data
        y : array-like, shape = (n_samples, [n_output_dims])
            Target values
        sample_weight : array-like, shape = [n_samples] or None
            Individual weights for each sample, ignored if None is passed.
        Returns
        -------
        self : object
            Returns self.
        """
        y_pred = self.model.fit(X, y, sample_weight).predict(X)
        if self.uncertainty_model is not None:
            error = np.abs(y - y_pred)
            self.prior = 2 * np.max(error)
            self.uncertainty_model.fit(X, error - self.prior)
        return self

    def predict_proba(self, X):
        """Predict using both models

        Parameters
        ----------
        X : array-like, shape = (n_samples, n_features)
            Samples
        Returns
        -------
        y_pred : array-like, shape = (n_samples,)
            Predicted values
        y_std : array-like, shape = (n_samples,)
        """
        y_pred = self.model.predict(X)
        if self.uncertainty_model is not None:
            y_std = self.uncertainty_model.predict(X) + self.prior
        else:
            y_std = np.ones(len(X))
        return y_pred, y_std
