#
# csea
# Copyright 2019 Maik Schmidt
#
#
# Deconvolution methods for Cherenkov astronomy and other use cases in experimental physics.
#
#
# csea is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with csea. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import beta
from scipy.stats.mstats import trimmed_mean, trimmed_std
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.model_selection import KFold


# Use cross-validation to get all predictions on the training data
def _cv_predictions(X_train, y_train, regressor, n_split=3):
    if hasattr(regressor, 'model') and hasattr(regressor.model, 'alpha') and isinstance(regressor.model.alpha,
                                                                                        np.ndarray):
        regressor.model.alpha = regressor.alpha
    kf = KFold(n_splits=n_split)
    y_pred = []
    for (train_idx, test_idx) in kf.split(X_train):
        y_pred.append(regressor.fit(X_train[train_idx], y_train[train_idx]).predict_proba(X_train[test_idx])[0])
    y_pred.reverse()
    return np.hstack(y_pred)


def get_moment_calibrator(X_train, y_train, regressor, robust=True):
    """Return a function object that calibrates predictions based of first and second moments

    Parameters
    ----------
    X_train : array-like, shape (n_training_samples,), nonnegative ints
        The observable quantity of the training set.

    y_train : array-like, shape (n_training_samples,), nonnegative ints
        The labels belonging to x_train.

    regressor : csea.Regressor
        The regressor object the calibration is computed for

    robust : bool, optional
        Whether to use robust statistics for mean and standard deviation.
        Defaults to false.

    """
    y_pred = _cv_predictions(X_train, y_train, regressor)
    if robust:
        train_mean, train_std = trimmed_mean(y_train), trimmed_std(y_train)
        pred_mean, pred_std = trimmed_mean(y_pred), trimmed_std(y_pred)
    else:
        train_mean, train_std = np.mean(y_train), np.std(y_train)
        pred_mean, pred_std = np.mean(y_pred), np.std(y_pred)

    shift = train_mean - pred_mean
    scale = train_std / pred_std
    #print(shift, scale)

    def _moment_calibrator(y, shift=shift, scale=scale):
        y_mean = np.mean(y)
        calibrated_y = (y - y_mean) * scale + shift + y_mean
        return calibrated_y

    return _moment_calibrator


def get_quantile_calibrator(X_train, y_train, regressor, n_quantiles, calibrator='gp', beta_shape=2, limit=20000):
    """Return a function object that calibrates predictions based on a quantile map

    Parameters
    ----------
    X_train : array-like, shape (n_training_samples,), nonnegative ints
        The observable quantity of the training set.

    y_train : array-like, shape (n_training_samples,), nonnegative ints
        The labels belonging to x_train.

    regressor : csea.Regressor
        The regressor object the calibration is computed for

    n_quantiles : int
        The number of values in the quantile map
        Should be the number of predictions, i.e. len(X_data)

    calibrator : str, optional
        The model fitted to to the P-P plot to get get quantile map
        'gp' fits a Gaussian process (default)
        'beta' fits the CDF of a beta distribution

    beta_shape : int, optional
        The alpha and beta value for the beta distribution in the
        variable noise Gaussian process. Defaults to 2.

    limit : int, optional
        This limits the size of the kernel matrix in the Gaussian process
        to limit x limit. Defaults to 20000.

    """
    y_pred = _cv_predictions(X_train, y_train, regressor)
    y_pred_sorted = np.sort(y_pred)
    y_train_sorted = np.sort(y_train)

    # Find quantiles of training points in the distribution of predictions
    y_pred_quantiles = np.linspace(0, 1, len(y_pred))
    y_train_quantiles = np.searchsorted(y_pred_sorted, y_train_sorted) / len(y_pred)

    # Limit size of kernel matrix
    if len(y_pred) > limit // 2:
        idx = np.linspace(0, len(y_pred) - 1, limit // 2, dtype=int)
        y_pred_quantiles = y_pred_quantiles[idx]
        y_train_quantiles = y_train_quantiles[idx]

    y_quantiles = np.linspace(0, 1, n_quantiles)
    if calibrator == 'gp':
        # Use Beta distributed noise to favor a good tail fit
        calibrator = GaussianProcessRegressor(normalize_y=True)
        sigma = beta.pdf(np.linspace(0, 1, min(len(y_pred), limit // 2)), beta_shape, beta_shape)
        calibrator.alpha = calibrator.alpha * (sigma + 0.1)  # / max(sigma)

        # Learn mapping from quantiles of predictions to the training quantiles
        calibrator.fit(y_pred_quantiles[:, np.newaxis], y_train_quantiles)
        if n_quantiles > limit // 2:
            y_quantiles_split = np.array_split(y_quantiles, np.ceil(n_quantiles / (limit // 2)))
            calibrated_quantiles = np.hstack([calibrator.predict(q[:, np.newaxis]) for q in y_quantiles_split])
        else:
            calibrated_quantiles = calibrator.predict(y_quantiles[:, np.newaxis])

    elif calibrator == 'beta':
        opt = curve_fit(beta.cdf, y_pred_quantiles, y_train_quantiles, p0=(2, 2), bounds=(0, np.inf))
        calibrated_quantiles = beta.cdf(y_quantiles, *opt[0])

    else:
        raise NotImplementedError

    calibrated_quantiles[calibrated_quantiles < 0] = 0
    calibrated_quantiles[calibrated_quantiles > 1] = 1

    def _quantile_calibrator(y, calibrated_quantiles=calibrated_quantiles):
        sort_idx = np.argsort(y)
        calibrated_y = np.quantile(y, calibrated_quantiles)
        calibrated_y = calibrated_y[np.argsort(sort_idx)]  # Undo sorting
        return calibrated_y

    return _quantile_calibrator
