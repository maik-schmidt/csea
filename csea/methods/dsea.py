#
# csea
# Copyright 2018, 2019 Mirko Bunse, Maik Schmidt
#
#
# Deconvolution methods for Cherenkov astronomy and other use cases in experimental physics.
#
#
# csea is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with csea. If not, see <http://www.gnu.org/licenses/>.
#
import numpy as np
import csea.util as util
from .. import (_recode_indices, _recode_result, _check_prior)


def deconvolve(X_data, X_train, y_train, classifier,
               bins = None,
               f_0 = None,
               fixweighting = True,
               alpha = 1,
               smoothing = None,
               K = 1,
               epsilon = 0.0,
               inspect = None,
               return_contributions = False):
    """Deconvolve the target distribution of X_data with DSEA, learning from X_train and
    y_train.

    Parameters
    ----------
    X_data : array-like, shape (n_samples, n_features), floats
        The data from which the target distribution is deconvolved.

    X_train : array-like, shape (n_samples_train, n_features), floats
        The data from which the classifier is trained.

    y_train : array-like, shape (n_samples_train,), nonnegative ints
        The indices of target quantity values belonging to X_train.

    classifier: object
        A classifier that is trained with classifier.fit(X_train, y_train, w_train) to
        obtain a matrix of probabilities with classifier.predict_proba(X_data).
        Any sklearn classifier is perfectly suited.

    bins: array-like, shape(I,), nonnegative ints, optional
        The indices of target quantity values. These values are allowed in y_train.

    f_0 : array-like, shape(I,), floats, optional
        The prior, which is uniform by default.

    fixweighting : bool, optional
        Whether or not the weight update fix is applied, which is proposed in my Master's
        thesis and the corresponding paper.

    alpha : float or function, optional
        A constant value or a function (k, pk, f_prev) -> float, which is used to choose
        the step size depending on the current estimate.

    smoothing : callable, optional
        A function (f) -> (f_smooth) optionally smoothing each estimate before using it as
        the prior of the next iteration.

    K : int, optional
        The maximum iteration number.

    epsilon : float, optional
        The minimum Chi Square distance between iterations. If the actual distance is below
        this threshold, convergence is assumed and the algorithm stops.

    inspect : callable, optional
        A function (f, k, alpha, chi2s) -> () optionally called in every iteration.

    return_contributions : bool, optional
        Whether or not to return the contributions of individual examples in X_data along
        with the deconvolution result.

    Returns
    ----------
    f : array-like, shape (I,)
        The estimated target pdf of X_data.

    contributions : array-like, shape (n_samples, I)
        The contributions of individual samples in X_data.
    """

    # check input data
    if bins is None:
        bins = np.unique(y_train)
    recode_dict, y_train = _recode_indices(bins, y_train)
    if X_data.shape[1] != X_train.shape[1]:
        raise ValueError("X_data and X_train have different numbers of features")
    elif not issubclass(y_train.dtype.type, np.integer):
        raise ValueError("dtype of y_train is not int")
    elif np.any(bins < 0):
        raise ValueError("y_train contains negative values")
    f_0 = _check_prior(f_0, recode_dict)

    # initial estimate
    f       = f_0
    f_train = np.bincount(y_train) / len(f_0)                            # training histogram
    w_train = _dsea_weights(y_train, f / f_train if fixweighting else f) # instance weights
    if inspect is not None:
        inspect(_recode_result(f, recode_dict), 0, np.nan, np.nan)

    # iterative deconvolution
    for k in range(1, K+1):
        f_prev = f.copy() # previous estimate

        # === update the estimate ===
        proba     = _train_and_predict_proba(classifier, X_data, X_train, y_train, w_train)
        f_next    = _dsea_reconstruct(proba) # original DSEA reconstruction
        f, alphak = _dsea_step(
          k,
          _recode_result(f_next, recode_dict),
          _recode_result(f_prev, recode_dict),
          alpha
        ) # step size function assumes original coding
        f = _check_prior(f, recode_dict) # re-code result of _dsea_step
        # = = = = = = = = = = = = = =

        # monitor progress
        chi2s = util.chi2s(f_prev, f) # Chi Square distance between iterations
        if inspect is not None:
            inspect(_recode_result(f, recode_dict), k, alphak, chi2s)

        # stop when convergence is assumed
        if chi2s < epsilon:
            break

        # == smoothing and reweighting in between iterations ==
        if k < K:
            if smoothing is not None:
                f = smoothing(f)
            w_train = _dsea_weights(y_train, f / f_train if fixweighting else f)
        # = = = = = = = = = = = = = = = = = = = = = = = = = = =

    f     = _recode_result(f,     recode_dict)
    proba = _recode_result(proba, recode_dict)
    return (f, proba) if return_contributions else f


def deconvolve_continuous(X_data, X_train, y_train, regressor, density_estimator,
               bins = 20,
               f_0 = None,
               fixweighting = True,
               alpha = 1,
               smoothing = None,
               K = 1,
               epsilon = 0.0,
               inspect = None,
               return_contributions = False,
               calibrator = None):
    """Continuously deconvolve the target distribution of X_data with CSEA, learning from
    X_train and y_train.

    Parameters
    ----------
    X_data : array-like, shape (n_samples, n_features), floats
        The data from which the target distribution is deconvolved.

    X_train : array-like, shape (n_samples_train, n_features), floats
        The data from which the classifier is trained.

    y_train : array-like, shape (n_samples_train,), nonnegative ints
        The indices of target quantity values belonging to X_train.

    regressor: object
        A regressor that is trained with regressor.fit(X_train, y_train, w_train) to
        obtain the prediction and their uncertainty with regressor.predict_proba(X_data).
        Use any csea.Regressor.

    density_estimator: object
        A density estimator that uses density_estimator.fit(y_pred, y_std) and
        density_estimator.predict(y_train) to get compute the re-weighting.
        Use any csea.DensityEstimator.

    bins: int or array-like
        If bins is int, it defines the number of equal-width bins in the given range.
        If bins is ndarray, it defines a monotonically increasing array of bin edges, including the
        rightmost edge, allowing for non-uniform bin widths.
        These values are used to discretize f for estimating the optimal step size.

    f_0 : array-like, shape(n_samples_train,), floats, optional
        The prior, which is uniform by default.

    fixweighting : bool, optional
        Whether or not the weight update fix is applied, which is proposed in Bunse's Master's
        thesis and the corresponding paper.

    alpha : float or function, optional
        A constant value or a function (k, pk, f_prev) -> float, which is used to choose
        the step size depending on the current estimate.

    smoothing : callable, optional
        A function (f) -> (f_smooth) optionally smoothing each estimate before using it as
        the prior of the next iteration.

    K : int, optional
        The maximum iteration number.

    epsilon : float, optional
        The minimum Chi Square distance between iterations. If the actual distance is below
        this threshold, convergence is assumed and the algorithm stops.

    inspect : callable, optional
        A function (f, k, alpha, chi2s) -> () optionally called in every iteration.

    return_contributions : bool, optional
        Whether or not to return the contributions of individual examples in X_data along
        with the deconvolution result.

    calibrator : function, optional
        The calibration function that calibrates the predictions

    Returns
    ----------
    f : array-like, shape (n_samples_train,)
        The estimated target pdf of X_data sampled at y_train

    contributions : array-like, shape (n_samples, 2)
        The contributions of individual samples in X_data,
        namely y_pred and y_std predicted by the regressor
    """

    adaptive_step_size = callable(alpha) and alpha.__name__ == 'alpha_adaptive_run'
    if isinstance(bins, int):
        bins = np.linspace(np.min(y_train), np.max(y_train), bins)
    # Convert bin intervals to bin representatives
    bins = np.array([(bins[i] + bins[i+1]) / 2 for i in range(len(bins)-1)])

    # check input data
    if X_data.shape[1] != X_train.shape[1]:
        raise ValueError("X_data and X_train have different numbers of features")
    f_0 = np.ones(len(y_train)) / (np.max(y_train) - np.min(y_train)) if f_0 is None else f_0

    # initial estimate
    f       = f_0                         # training histogram
    w_train = f                           # instance weights
    if inspect is not None:
        inspect(f, 0, np.nan, np.nan)

    _, f_dens_train = density_estimator.fit(y_train).predict(y_train)
    if adaptive_step_size:
        bin_representatives = _get_representatives(y_train, bins)
        f_discrete_prev = util.normalizepdf(f[bin_representatives])  # Compute initial discretization numerically
    w_train = f / f_dens_train if fixweighting else f

    # iterative deconvolution
    for k in range(1, K+1):
        f_prev = f.copy() # previous estimate

        # === update the estimate ===
        y_pred, y_std = _train_and_predict_proba(regressor, X_data, X_train, y_train, w_train)
        y_std = np.nan_to_num(y_std, copy=False)

        # Calibrate predictions
        if calibrator is not None:
            y_pred = calibrator(y_pred)

        # Estimate target density to get weights of training samples
        _, f_dens_obs = density_estimator.fit(y_pred, y_std).predict(y_train)
        # Approximate discrete target density for step size computation
        if adaptive_step_size:
            f_discrete = util.normalizepdf(density_estimator.predict(bins)[1])
            f, f_next, alphak = _dsea_step_continuous(k, f_dens_obs, f_prev, f_discrete, f_discrete_prev, alpha)
        else:
            f, alphak = _dsea_step(k, f_dens_obs, f_prev, alpha)
        # = = = = = = = = = = = = = =

        # monitor progress
        chi2s = util.chi2s(f_prev, f) # Chi Square distance between iterations
        if inspect is not None:
            inspect(f, k, alphak, chi2s)

        # stop when convergence is assumed
        if chi2s < epsilon:
            break

        # == smoothing and reweighting in between iterations ==
        if k < K:
            if smoothing is not None:
                f = smoothing(f)
            w_train = f / f_dens_train if fixweighting else f
        # = = = = = = = = = = = = = = = = = = = = = = = = = = =

    return f, np.column_stack((y_pred, y_std)) if return_contributions else f


# the weights of training instances are based on the bin weights in w_bin
def _dsea_weights(y_train, w_bin, normalize = True):
    if normalize:
        w_bin = util.normalizepdf(w_bin) # normalized copy
    return np.maximum(w_bin[y_train], 1/len(y_train)) # Laplace correction


# train and apply the classifier to obtain a matrix of confidence values
def _train_and_predict_proba(classifier, X_data, X_train, y_train, w_train):
    classifier.fit(X_train, y_train, w_train)
    return classifier.predict_proba(X_data)


# the reconstructed estimate is the normalized sum of confidences in each bin
def _dsea_reconstruct(proba):
    return util.normalizepdf(np.apply_along_axis(np.sum, 0, proba))


# the step taken by DSEA+, where alpha may be a constant or a function
def _dsea_step(k, f, f_prev, alpha):
    pk     = f - f_prev                                         # search direction
    alphak = alpha(k, pk, f_prev) if callable(alpha) else alpha # function or constant
    return f_prev + alphak * pk,  alphak                        # estimate and step size


# the step taken by CSEA, where alpha may be a constant or a function
def _dsea_step_continuous(k, f, f_prev, f_discrete, f_discrete_prev, alpha):
    pk_discrete = f_discrete - f_discrete_prev
    pk = f - f_prev                                                                           # search direction
    alphak = alpha(k, pk_discrete, f_discrete_prev, pk, f_prev) if callable(alpha) else alpha # function or constant
    return f_prev + alphak * pk, f_discrete_prev + alphak * pk_discrete, alphak               # estimates and step size


# Find left-next neighbor of each bin representative in f
def _get_representatives(f, bins):
    sort_idx = np.argsort(f)
    f_sorted = f[sort_idx][1:]  # Remove first element to get the next smaller element with searchsorted
    bin_idx = np.searchsorted(f_sorted, bins)
    return sort_idx[bin_idx]  # Searchsorted computes index for sorted array, need to reverse
