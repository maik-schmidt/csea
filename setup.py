from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='csea',
    version='0.0.1',
    author='Maik Schmidt, Mirko Bunse',
    author_email='maik.schmidt@tu-dortmund.de',
    description='Deconvolution methods for Cherenkov astronomy and other use cases in experimental physics.',
    long_description=readme,
    url='https://bitbucket.org/maik-schmidt/csea',
    license=license,
    packages=find_packages(exclude=('docs'))
)
